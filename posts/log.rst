.. title: 管理日志
.. slug: log
.. date: 2020-11-07 22:53:55 UTC+08:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text

帐号
======================

.. csv-table:: 
   :header: "账号", "操作类型", "操作时间", "操作原因", "备注"

    `wuliaotu@eggp.me <https://eggp.me/@wuliaotu>`_ ,silenced,2019-03-08,NSFW内容不加CW，多次通知后仍无改变,eggp.me 实例目前已无法访问（2019-06-05）
    `KramaTomaiyoko@acg.mn <https://acg.mn/@KramaTomaiyoko>`_ ,suspend,2019-03-30,Spammer,该账号已被 acg.mn 实例注销
    `relay_bot <https://bgme.me/@relay_bot>`_ ,silenced,2019-04-26,账号所有者要求,
    `Qanon99@pl.smuglo.li <https://pl.smuglo.li/users/Qanon99>`_ ,suspended,2019-06-27,Spammer,该账号已被 pl.smuglo.li 封禁
    `KekLover666@shitposter.club <https://shitposter.club/users/KekLover666>`_ ,suspended,2019-06-27,Spammer,该账号已被 shitposter.club 封禁
    `CreamPuff88@mastodon.xyz <https://mastodon.xyz/@CreamPuff88>`_ ,suspended,2019-07-01,Spammer,该账号已被 mastodon.xyz 实例注销
    `MySexyGirlsPics@humblr.social <https://humblr.social/@MySexyGirlsPics>`_ ,silenced,2019-07-10,NSFW内容不加CW,
    `GujoMein@mastodon.social <https://mastodon.social/@GujoMein>`_ ,suspended,2019-07-12,Spammer,该帐号已被所在实例注销（2020-11-8）
    `justoneshotinthedark@humblr.social <https://pawoo.net/@AriaLAnna69>`_ ,silenced,2019-07-23,NSFW内容未加CW,
    `AriaLAnna69@pawoo.net <https://pawoo.net/@AriaLAnna69>`_ ,suspended,2019-09-20,Spammer,该帐号已被所在实例注销（2020-11-8）
    `sayuki90@qiitadon.com <https://qiitadon.com/@sayuki90>`_ ,suspended,2019-09-20,Spammer,该帐号已被所在实例注销（2020-11-8）
    `Hanakooo31@pawoo.net <https://pawoo.net/@Hanakooo31>`_ ,suspended,2019-09-22,Spammer,该帐号已被所在实例注销（2020-11-8）
    `Naomii55@pawoo.net <https://pawoo.net/@Naomii55>`_ ,suspended,2019-09-22,Spammer,该帐号已被所在实例注销（2020-11-8）
    `Hanakooo13@pawoo.net <https://pawoo.net/@Hanakooo13>`_ ,suspended,2019-09-23,Spammer,该帐号已被所在实例注销（2020-11-8）
    `Haanakko96@pawoo.net <https://pawoo.net/@Haanakko96>`_ ,suspended,2019-11-06,Spammer,该帐号已被所在实例注销（2020-11-8）
    `Naomii60@pawoo.net <https://pawoo.net/@Naomii60>`_ ,suspended,2019-11-06,Spammer,该帐号已被所在实例注销（2020-11-8）
    `AriaLAnna76@pawoo.net <https://pawoo.net/@AriaLAnna76>`_ ,suspended,2019-11-06,Spammer,该帐号已被所在实例注销（2020-11-8）
    `oneleven@mastodon.social <https://mastodon.social/@oneleven>`_ ,suspended,2019-11-28,Spammer,该帐号已被所在实例注销（2020-11-8）
    `aramaki@mastodon.social <https://mastodon.social/@aramaki>`_ ,suspended,2019-12-01,Spammer,该帐号已被所在实例注销（2020-11-8）
    `AsianDating@sinblr.com <https://sinblr.com/@AsianDating>`_ ,silenced,2019-12-16,NSFW内容未加CW,
    `Emma@sexcify.com <https://sexcify.com/author/emma>`_ ,silenced,2020-01-02,NSFW内容未加CW,
    `Hulu@m.cmx.im <https://m.cmx.im/@Hulu>`_ ,silenced,2020-02-03,NSFW头像,通过\ `所在实例 Profile <https://m.cmx.im/@Hulu>`_ 查看该用户资料，其头像为\ `缺省头像 <https://m.cmx.im/avatars/original/missing.png>`_，但通过bgme实例查看仍为原头像，故继续限制（2020-11-8）
    `hurricanelantern@sinblr.com <https://sinblr.com/@hurricanelantern>`_ ,silenced,2020-02-07,NSFW头像,
    `saralovessex28@sinblr.com <https://sinblr.com/@saralovessex28>`_ ,silenced,2020-02-16,NSFW头像,已删号（2020-11-7）
    `future@crypto-group-buy.com <https://crypto-group-buy.com/@future>`_ ,suspended,2020-02-17,Spammer,实例已下线（2020-11-7）
    `winner@crypto-group-buy.com <https://crypto-group-buy.com/@winner>`_ ,suspended,2020-02-17,Spammer,实例已下线（2020-11-7）
    `jandan_pic@bots.tinysubversions.com <https://bots.tinysubversions.com/u/jandan_pic>`_ ,suspended,2020-02-18,影响实例正常工作,机器人帐号，未知原因影响ES索引
    `tareq236@mastodon.social <https://mastodon.social/@tareq236>`_ ,suspended,2020-05-08,Spammer,该帐号已被所在实例注销（2020-11-8）
    `vertibond@sinblr.com <https://sinblr.com/@vertibond>`_ ,silenced,2020-05-24,NSFW内容未加CW,
    `bi********@wxw.moe <https://wxw.moe/@bi********>`_ ,suspended,2020-05-31,Child pornography,该帐号已被所在实例注销（2020-11-8）
    `et********@mstdn.jp <https://mstdn.jp/@et********>`_ ,suspended,2020-05-31,Child pornography,该帐号已被所在实例注销（2020-11-8）
    `Ce********@pawoo.net <https://pawoo.net/@Ce********>`_ ,suspended,2020-05-31,Child pornography,该帐号已被所在实例注销（2020-11-8）
    `sh********@pawoo.net <https://pawoo.net/@sh********>`_ ,suspended,2020-05-31,Child pornography,该帐号已被所在实例注销（2020-11-8）
    `So********@pawoo.net <https://pawoo.net/@So********>`_ ,suspended,2020-05-31,Child pornography,该帐号已被所在实例注销（2020-11-8）
    `ka********@mstdn.jp <https://mstdn.jp/@ka********>`_ ,suspended,2020-05-31,Child pornography,该帐号已被所在实例注销（2020-11-8）
    `cr********@pawoo.net <https://pawoo.net/@cr********>`_ ,suspended,2020-05-31,Child pornography,该帐号已被所在实例注销（2020-11-8）
    `onehp@mastodon.social <https://mastodon.social/@onehp>`_ ,suspended,2020-06-03,Spammer,该帐号已被所在实例注销（2020-11-8）
    `Chocrates@donotban.com <https://donotban.com/@Chocrates>`_ ,suspended,2020-08-10,歧视+威胁言论,
    `FetishAss@sinblr.com <https://sinblr.com/@FetishAss>`_ ,silenced,2020-08-10,NSFW头像,
    `onm89285 <https://bgme.me/@onm89285>`_ ,disabled login,2020-09-14,骚扰行为,详细情况可参考：https://bgme.me/@admin/104864234236908772 （2020-11-8）
    `JustinRess@switter.at <https://switter.at/@JustinRess>`_ ,suspended,2020-09-24,Spammer,
    `AiShinozaki@gameliberty.club <https://gameliberty.club/@AiShinozaki>`_ ,silenced,2020-11-07,NSFW内容未加CW,
    `luda@o3o.ca <https://o3o.ca/@luda>`_ ,silenced,2020-11-21,NSFW内容未加CW,通告嘟文： https://bgme.me/@admin/105247536745840264 （2020-11-22）
    `maki@ranran.tw <https://ranran.tw/@maki>`_ ,silenced,2020-11-22,NSFW内容未加CW,
    `Snow@gleasonator.com <https://gleasonator.com/users/Snow>`_,suspended,2020-11-29,骚扰行为,详细情况可参考：https://bgme.me/@admin/105292264354892005 （2020-11-29）
    `Snow@freespeechextremist.com  <https://freespeechextremist.com/users/Snow>`_,suspended,2020-11-29,骚扰行为,详细情况可参考：https://bgme.me/@admin/105292264354892005 （2020-11-29）
    `Snow@blob.cat   <https://blob.cat/users/Snow>`_,suspended,2020-11-29,骚扰行为,详细情况可参考：https://bgme.me/@admin/105292264354892005 （2020-11-29）
    `snow@qoto.org   <https://qoto.org/@snow>`_,suspended,2020-11-29,骚扰行为,详细情况可参考：https://bgme.me/@admin/105292264354892005 （2020-11-29）
    `snow@wintermute.fr.to  <https://wintermute.fr.to/users/snow>`_,suspended,2020-11-29,骚扰行为,详细情况可参考：https://bgme.me/@admin/105292264354892005 （2020-11-29）
    `@newsbot@dajiaweibo.com  <https://dajiaweibo.com/@newsbot>`_,silenced,2020-12-04,刷屏,详细情况参见：https://bgme.me/@admin/105322535439139631 （2020-12-04）
    `teencumm@xxxtumblr.org  <https://xxxtumblr.org/@teencumm>`_ ,silenced,2021-01-11,NSFW内容未加CW,
    `ariroseQQ@switter.at <https://switter.at/@ariroseQQ>`_ ,suspended,2021-01-14,Spammer,
    `Pussymaniac@xxxtumblr.org  <https://xxxtumblr.org/@Pussymaniac>`_ ,silenced,2021-02-27,NSFW内容未加CW,
    `ying99@mastodon.social   <https://mastodon.social/@ying99>`_ ,suspended,2021-04-08,辱骂骚扰行为,详情可参：https://bgme.me/@admin/106025089177445487
    `ying_222@mastodon.online  <https://mastodon.online/@ying_222>`_ ,suspended,2021-04-08,辱骂骚扰行为,详情可参：https://bgme.me/@admin/106025089177445487
    `NMSL@mas.to  <https://mas.to/@NMSL/>`_ ,suspended,2021-04-08,辱骂骚扰行为,详情可参：https://bgme.me/@admin/106029588042108265
    `eposicee@mastodon.uno  <https://mastodon.uno/@eposicee/>`_ ,suspended,2021-04-11,恶意冒用他人身份以及骚扰行为,详情可参：https://bgme.me/@admin/106044492040125516
    `Edafealex@mastodon.social  <https://mastodon.social/@Edafealex>`_ ,suspended,2021-05-22,Spammer,详情可参：https://bgme.me/@admin/106274129761935606
    `magma@9kb.me  <https://9kb.me/@magma/>`_ ,suspended,2021-06-17,骚扰行为,详情可参：https://bgme.me/@admin/106426190079293292
    `cheung_123@mastodon.social  <https://mastodon.social/@cheung_123>`_ ,suspended,2021-07-10,骚扰辱骂行为,详情可参：https://bgme.me/@admin/106554554862515295
    `cheung_123@mastodon.social  <https://mastodon.social/@helicopter_hao_chou>`_ ,suspended,2021-07-10,骚扰辱骂行为,详情可参：https://bgme.me/@admin/106554554862515295
    `zhongRRR@mastodon.social  <https://mastodon.social/@zhongRRR/>`_ ,suspended,2021-07-12,辱骂行为,详情可参：https://bgme.me/@admin/106567976384369919



实例
=======================

请参见\ `被限制的服务器 <https://bgme.me/about/more#unavailable-content>`_ 。

----------------------------

管理日志最后更新于：2021年01月14日


.. |date| date::
.. |time| date:: %H:%M

本文档最后生成于： |date| |time|
