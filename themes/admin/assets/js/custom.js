$(document).ready(function() {
    openLinkNewTab();
    replaceLink();

    function openLinkNewTab() {
        document.querySelectorAll('.reference.external').forEach(a => handle(a));

        function handle(a) {
            if (a.href == '') {
                return
            }

            let domain = (new URL(a.href)).host;
            if (domain != document.location.host) {
                a.rel = 'noopener noreferrer';
                a.target = '_blank';
            }
        }
    }

    function replaceLink() {
        const ss = ['a.footnote-reference', '.fn-backref a', 'a.fn-backref'];
        for (let s of ss) {
            document.querySelectorAll(s).forEach(a => a.href = a.href.replace(`${document.location.origin}/posts/tos/`,''));
        }
    }
})
